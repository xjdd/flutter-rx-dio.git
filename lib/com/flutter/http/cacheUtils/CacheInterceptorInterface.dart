import 'package:flutter_dio_module/lib_dio.dart';

/// 缓存拦截器的抽象类
abstract class CacheInterceptorInterface implements InterceptorsWrapper {
  String path = "";
  Map<String, dynamic>? map;

  //错误处理
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    return handler.next(err);
  }

  ///拦截请求
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    path = options.path;
    if (map != null && map!.isNotEmpty) {
      //没有param的时候是一个空的对象
      map = options.queryParameters;
    } else {
      map = null;
    }
    return handler.next(options);
  }

  ///拦截返回值
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    if (RxDioConfig.instance.getCacheState()) {
      saveCache(path, map, response.data);
    }
    return handler.next(response);
  }

  saveCache(String path, Map<String, dynamic>? params, String responseData);
}
