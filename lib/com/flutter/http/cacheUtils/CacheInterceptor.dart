import '../RxDioConfig.dart';
import 'CacheInterceptorInterface.dart';

///缓存拦截器 缓存请求连接,请求参数, 返回数据
class CacheInterceptor extends CacheInterceptorInterface {
  @override
  saveCache(String path, Map<String, dynamic>? params, String responseData) {
    RxDioConfig.instance
        .getCacheInterface()
        ?.saveCache(path, params, responseData);
  }
}
