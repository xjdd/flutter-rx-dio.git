///网络请求模型,默认支持:Get,Post,Put,Delete
enum Method {
  /// get request
  Get,

  /// post request
  Post,

  /// put request
  Put,

  /// delete request
  Delete
}
